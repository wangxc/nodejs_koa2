const Router = require('koa-router')
const {Flow} = require('../../models/flow')
const router = new Router({
	prefix: '/v1/classic'
})

// const {HttpException,ParameterException} =require("../../../core/http-exception")
const {
	PositiveIntegerValidator
} = require("../../validators/validator")
const {
	Auth
} = require("../../../middlewares/auth")
const {
	Art
} = require("../../models/art")

//定义路由
router.get('/latest', new Auth().m, async (ctx, next) => {
	//权限
	// 权限 token 角色
	//分级 scope
	//8 普通用户 16 管理员

	const flow =await Flow.findOne({
		order: [
			['index','DESC']
		]
	})
	const art=await Art.getData(flow.art_id,flow.type)
	// art.dataValues.index=flow.index
	art.setDataValue('index',flow,index)
	ctx.body = art

})

module.exports = router
