const Koa=require('koa')
const InitManager=require('./core/init')
const parser=require('koa-bodyparser')
const catchError=require('./middlewares/exception')
// const book=require('./api/v1/book')
// const classic=require('./api/v1/classic')

// require('./app/models/user')

const app=new Koa();
app.use(parser())
app.use(catchError)
InitManager.initCore(app)

app.listen(3000)

// const router=new Router()
//应用程序对象 中间件

//发送HTTP KOA 接收HTTP

//函数
// function test(){
// 	console.log('hello,7yue');	
// }

//注册
// app.use(async (ctx,next)=>{
// 	//上下文ctx,下一个中间件函数next
// 	console.log('1');
//	//Promise async await
// 	const a=await next()
// 	//求值关键字
// 	console.log(a)
// 	// a.then((res)=>{
// 	// 	console.log(res)
// 	// })
// 	console.log('2');
// })

// app.use(async (ctx,next)=>{
// 	console.log(ctx.path);
// 	console.log(ctx.method);

// 	//路由
// 	if(ctx.path==="/classic/latest" && ctx.method==="GET"){
// 		ctx.body= {"key":"classic"}  //返回值
// 	}
// })

//定义路由
// router.get('/classic/latest',(ctx,next)=>{
// 	ctx.body= {"key":"classic"}  //返回值
// })

//扫描路由
// requireDirecotry(module,"./app/api",{visit:whenLoadModule})

// function whenLoadModule(obj){
// 	if(obj instanceof Router){
// 		app.use(obj.routes())
// 	}
// }



// app.use(router.routes())//注册路由
// app.use(book.routes())//注册路由
// app.use(classic.routes())


