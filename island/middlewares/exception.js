const {HttpException} =require("../core/http-exception")

const catchError= async(ctx,next)=>{
	try{
		await next()
	}catch(e){
		//开发环境
		
		const isHttpException=e instanceof HttpException
		console.log("------------------2")
		console.log(isHttpException)
		const isDev=global.config.environment==='dev'
		
		if(isDev && !isHttpException){
			throw e
		}
		//生产环境
		if(isHttpException){
			//已知错误
			ctx.body={
				msg:e.msg,
				error_code:e.errorCode,
				request:`${ctx.method} ${ctx.path}`
			}
			ctx.status=e.code
		}
		else{
			//未知错误
			ctx.body={
				msg:'we made a mistake 0(n_n)0~~',
				error_code:999,
				request:`${ctx.method} ${ctx.path}`
			}
			ctx.status=500
		}
		//message
		//error_code 详细,开发者定义的信息
		//request_url 当前请求的url 
		
	}
}

module.exports=catchError