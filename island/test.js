function func1(){
	func2()
}

async function func2(){
	try{
		await func3()
	}catch(error){
		console.log("error");
	}
}

function func3(){
	return new Promise((resolve,reject)=>{
		setTimeout(function(){
			const r= Math.random()
			if(r<0.5){
				//异常调用返回
				reject('error async')
			}else{
				//正常调用返回
			}
		})
	})
	// await setTimeout(function(){
	// 	throw new Error("error")
	// });
}

func1()

