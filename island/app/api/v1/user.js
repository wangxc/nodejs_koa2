const bcrypt=require('bcryptjs')
const Router=require('koa-router')

const {success}=require('../../lib/helper')

const {ResiterValidator}=require('../../validators/validator')
const {User}=require('../../models/user')
const router=new Router({
	prefix:'/v1/user'
})

//注册

router.post('/register',async(ctx)=>{
	//思维路径
	//接收参数 LinValidator
	//email password1 password2 nickname
	const v=await new ResiterValidator().validate(ctx)
	
	
	//令牌获取 颁布令牌
	const user={
		email:v.get('body.email'),
		password:v.get('body.password2'),
		nickname:v.get('body.nickname'),
		createdAt:new Date(),
		updatedAt:new Date(),
	}
	
	await User.create(user)
	success()
});

module.exports=router