const {sequelize}=require('../../core/db')
const {Sequelize,Model}=require('sequelize')

const classicFields={
	image:Suquelize.STRING,
	content:Suquelize.STRING,
	pubdate:Suquelize.DATEONLY,
	fav_nums:Suquelize.INTEGER,
	title:Suquelize.STRING,
	type:Suquelize.TINYINT,
}

class Movie extends Model{
	
}

Movie.init(classicFields,{
	sequelize,
	tableName:'movie'
})


class Sentence extends Model{
	
}

Sentence.init(classicFields,{
	sequelize,
	tableName:'sentence'
})


class Music extends Model{
	
}

const musicFields=Object.assign({url:Sequelize.STRING},classicFields}

Music.init(musicFields,{
	sequelize,
	tableName:'music'
})
module.exports={
	Movie,
	Sentence,
	Music
}