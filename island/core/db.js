const Sequelize=require('sequelize')
const{
	dbName,
	host,
	port,
	user,
	password
}=require('../config/config').database

const sequelize=new Sequelize(dbName,user,password,{
	dialect:'mysql',
	host,
	port,
	logging:true, //设置日志显示
	timezone:'+08:00',
	define:{
		
	}
})

sequelize.sync({
	force:false //是否重置数据库
})

module.exports={
	sequelize
}